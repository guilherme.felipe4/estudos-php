<?php



enum ZoomMode
{
  case small;
  case medium;
  case big;
  case superBig;
}

class Calendar
{
  private ZoomMode $zoomMode;

  public function setZoomMode(ZoomMode $mode)
  {
    $this->zoomMode = $mode;
  }
  public function Show()
  {
    if ($this->zoomMode == ZoomMode::small) {
      print "Zoom Pequeno";
    } else if ($this->zoomMode == ZoomMode::medium) {
      print "Zoom Medio";
    } else if ($this->zoomMode == ZoomMode::big) {
      print "Zoom Grande";
    } else {
      print 'Zoom Super Grande';
    }
  }
}

$calendar = new Calendar();
$calendar->setZoomMode(ZoomMode::small);
$calendar->Show();
