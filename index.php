<?php

function calcular_IMC(float $altura, float $peso): int
{
  return $peso / ($altura * $altura);
}

$result = calcular_IMC(1.93, 104);

echo "Seu IMC é de {$result}";
echo "\n";

function Km2Miles($km){
  return $km * 0.6;
}

echo Km2Miles(100);